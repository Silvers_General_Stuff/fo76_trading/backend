FROM node:13 as build_stage
# Create app directory
WORKDIR /app_build
# Install app dependencies
COPY package.json ./
COPY yarn.lock ./

RUN yarn install
# Copy app source code
COPY . .

# copy to a smaller container
FROM node:13-alpine
COPY --from=build_stage /app_build /app
WORKDIR /app
#Expose port and start application
EXPOSE 8080
CMD [ "yarn", "start" ]