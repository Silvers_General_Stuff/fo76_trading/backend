import { Legendary_Effects } from "./db_entities"
import {Connection} from "typeorm";

// from https://fallout.fandom.com/wiki/Fallout_76_legendary_effects
let legendary_effects: Legendary_Effects[] = [
  // Weapon
  // Prefixes
  { id: "005281b4", position: 1, name: "Anti-Armor", description: "Ignores 50% of your target's armor.", restriction: "Weapon", restriction_sub: null, restriction_unique: [] },
  { id: "004f5770", position: 1, name: "Assassin's", description: "+10% damage to players.", restriction: "Weapon", restriction_sub: null, restriction_unique: [] },
  { id: "004f6aa7", position: 1, name: "Berserker's", description: "Deal more damage the lower your Damage Resistance.", restriction: "Weapon", restriction_sub: null, restriction_unique: [] },
  { id: "004f6aa0", position: 1, name: "Bloodied", description: "Does more damage the lower your health.", restriction: "Weapon", restriction_sub: null, restriction_unique: [] },
  { id: "004392cd", position: 1, name: "Double", description: "Double ammo capacity", restriction: "Weapon", restriction_sub: "Ranged", restriction_unique: [] },
  { id: "004f6aa1", position: 1, name: "Executioner's", description: "+50% damage when your target is below 40% health.", restriction: "Weapon", restriction_sub: null, restriction_unique: [] },
  { id: "001f81eb", position: 1, name: "Exterminator's", description: "+30% damage against Mirelurks and bugs.", restriction: "Weapon", restriction_sub: null, restriction_unique: [] },
  { id: "004f577d", position: 1, name: "Furious", description: "Damage increased after each consecutive hit on the same target.", restriction: "Weapon", restriction_sub: null, restriction_unique: [] },
  { id: "004f5779", position: 1, name: "Ghoul Slayer's", description: "Does 30% more damage against Ghouls.", restriction: "Weapon", restriction_sub: null, restriction_unique: [] },
  { id: "004f577a", position: 1, name: "Hunter's", description: "Does 30% more damage against animals.", restriction: "Weapon", restriction_sub: null, restriction_unique: [] },
  { id: "004f6aa5", position: 1, name: "Instigating", description: "Does double damage if the target is at full health.", restriction: "Weapon", restriction_sub: null, restriction_unique: [] },
  { id: "004f6aab", position: 1, name: "Junkie's", description: "Deal more damage the more chem withdrawal effects you currently have.", restriction: "Weapon", restriction_sub: null, restriction_unique: [] },
  { id: "00527f8b", position: 1, name: "Medic's", description: "V.A.T.S. crits will heal you and your group.", restriction: "Weapon", restriction_sub: "Ranged", restriction_unique: [] },
  { id: "005299f5", position: 1, name: "Mutant's", description: "Damage increased by 10% if you are mutated.", restriction: "Weapon", restriction_sub: null, restriction_unique: [] },
  { id: "004f577b", position: 1, name: "Mutant Slayer's", description: "+30% damage to Super Mutants.", restriction: "Weapon", restriction_sub: null, restriction_unique: [] },
  { id: "004f6aae", position: 1, name: "Nocturnal", description: "Does increasing amounts of damage as the night grows longer, but less damage during the day.", restriction: "Weapon", restriction_sub: null, restriction_unique: [] },
  { id: "004f6ab1", position: 1, name: "Quad", description: "Quadruple ammo capacity", restriction: "Weapon", restriction_sub: "Ranged", restriction_unique: [] },
  { id: "004f6d77", position: 1, name: "Stalker's", description: "If not in combat, +100% V.A.T.S. accuracy at +50% AP cost. V.A.T.S.", restriction: "Weapon", restriction_sub: null, restriction_unique: [] },
  { id: "005281b8", position: 1, name: "Suppressor's", description: "Reduce your target's damage output by 20% for 3 seconds.", restriction: "Weapon", restriction_sub: null, restriction_unique: [] },
  { id: "004f577c", position: 1, name: "Troubleshooter's", description: "+30% damage to robots", restriction: "Weapon", restriction_sub: null, restriction_unique: [] },
  { id: "004f6d76", position: 1, name: "Two Shot", description: "Shoots an additional projectile.", restriction: "Weapon", restriction_sub: "Ranged", restriction_unique: [] },
  { id: "00527f84", position: 1, name: "Vampire's", description: "Gain brief health regeneration when you hit an enemy.", restriction: "Weapon", restriction_sub: null, restriction_unique: [] },
  { id: "004ed02b", position: 1, name: "Zealot's", description: "+30% damage to Scorched", restriction: "Weapon", restriction_sub: null, restriction_unique: [] },
  // Major modifiers
  {id: "005299f9", position: 2, name: "Basher's", description: "Bashing damage increased by 40%", restriction: "Weapon", restriction_sub: "Ranged", restriction_unique: [] },
  {id: "004f5771", position: 2, name: "Explosive", description: "Bullets explode for area damage (20% base dmg)", restriction: "Weapon", restriction_sub: "Ranged", restriction_unique: [] },
  {id: "0052414e", position: 2, name: "Hitman's", description: "+10% damage while aiming", restriction: "Weapon", restriction_sub: "Ranged", restriction_unique: [] },
  {id: "0052414b", position: 2, name: "Lucky", description: "V.A.T.S. critical shots do +50% damage", restriction: "Weapon", restriction_sub: "Ranged", restriction_unique: [] },
  {id: "0052414f", position: 2, name: "Rapid", description: "25% faster fire rate", restriction: "Weapon", restriction_sub: "Ranged", restriction_unique: [] },
  {id: "00524153", position: 2, name: "VATS Enhanced", description: "+33% VATS hit chance", restriction: "Weapon", restriction_sub: "Ranged", restriction_unique: [] },
  {id: "001a7bda", position: 2, name: "Melee Speed", description: "40% increased swing speed", restriction: "Weapon", restriction_sub: "Melee", restriction_unique: [] },
  {id: "001a7be2", position: 2, name: "Power Attack Damage", description: "40% more power attack damage", restriction: "Weapon", restriction_sub: "Melee", restriction_unique: [] },
  {id: "001a7c39", position: 2, name: "Riposting", description: "Reflects 50% of melee damage back while blocking", restriction: "Weapon", restriction_sub: "Melee", restriction_unique: [] },
  {id: "004ed02c", position: 2, name: "Crippling", description: "+50% limb damage", restriction: "Weapon", restriction_sub: null, restriction_unique: [] },
  // Minor mods
  { id: "0052414c", position: 3, name: "Lucky", description: "Your V.A.T.S. critical meter fills 15% faster", restriction: "Weapon", restriction_sub: "Ranged", restriction_unique: [] },
  { id: "004ed02e", position: 3, name: "Nimble", description: "Faster movement speed while aiming", restriction: "Weapon", restriction_sub: "Ranged", restriction_unique: [] },
  { id: "005299fa", position: 3, name: "Perception", description: "+1 Perception", restriction: "Weapon", restriction_sub: "Ranged", restriction_unique: [] },
  { id: "00524150", position: 3, name: "Rapid", description: "15% faster reload", restriction: "Weapon", restriction_sub: "Ranged", restriction_unique: [] },
  { id: "004f5777", position: 3, name: "Resilient", description: "+250 Damage Resistance while reloading", restriction: "Weapon", restriction_sub: "Ranged", restriction_unique: [] },
  { id: "004f5772", position: 3, name: "Steadfast", description: "+50 Damage resistance while aiming", restriction: "Weapon", restriction_sub: "Ranged", restriction_unique: [] },
  { id: "00524154", position: 3, name: "VATS Enhanced", description: "25% less V.A.T.S. Action Point cost", restriction: "Weapon", restriction_sub: "Ranged", restriction_unique: [] },
  { id: "005253fb", position: 3, name: "Cavalier's", description: "Take 15% less damage while blocking", restriction: "Weapon", restriction_sub: "Melee", restriction_unique: [] },
  { id: "001a7bd3", position: 3, name: "Cavalier's Icon sic", description: "Take 40% less damage while power attacking", restriction: "Weapon", restriction_sub: "Melee", restriction_unique: [] },
  { id: "005299fd", position: 3, name: "Endurance", description: "+1 Endurance", restriction: "Weapon", restriction_sub: "Melee", restriction_unique: [] },
  { id: "005299fc", position: 3, name: "Strength", description: "+1 Strength", restriction: "Weapon", restriction_sub: "Melee", restriction_unique: [] },
  { id: "005299fb", position: 3, name: "Agility", description: "+1 Agility", restriction: "Weapon", restriction_sub: null, restriction_unique: [] },
  { id: "00524152", position: 3, name: "Lightweight", description: "90% reduced weight", restriction: "Weapon", restriction_sub: null, restriction_unique: [] },
  // Armor
  // Prefix
  { id: "004f6d78", position: 1, name: "Assassin's", description: "75% chance to reduce damage by 8% from players", restriction: "Armor", restriction_sub: null, restriction_unique: [] },
  { id: "00521915", position: 1, name: "Auto Stim", description: "Automatically use a Stimpak while health is 25% or less, once every 60 seconds", restriction: "Armor", restriction_sub: null, restriction_unique: [] },
  { id: "00521914", position: 1, name: "Bolstering", description: "Grants up to +35 Energy Resistance and Damage Resistance, the lower your health", restriction: "Armor", restriction_sub: null, restriction_unique: [] },
  { id: "00524146", position: 1, name: "Chameleon", description: "Blend with the environment while sneaking and not moving", restriction: "Armor", restriction_sub: null, restriction_unique: [] },
  { id: "00524147", position: 1, name: "Cloaking", description: "Being hit in melee generates a Stealth field once per 30 seconds", restriction: "Armor", restriction_sub: null, restriction_unique: [] },
  { id: "004f6d7c", position: 1, name: "Exterminator's", description: "-15% damage from Mirelurks and bugs", restriction: "Armor", restriction_sub: null, restriction_unique: [] },
  { id: "004f6d7e", position: 1, name: "Ghoul Slayer's", description: "-15% damage from ghouls", restriction: "Armor", restriction_sub: null, restriction_unique: [] },
  { id: "004f6d7d", position: 1, name: "Hunter's", description: "-15% damage from animals", restriction: "Armor", restriction_sub: null, restriction_unique: [] },
  { id: "00529a0f", position: 1, name: "Life saving", description: "While incapacitated, gain a 50% chance to revive yourself with a Stimpak, once every minute", restriction: "Armor", restriction_sub: null, restriction_unique: [] },
  { id: "00529a0c", position: 1, name: "Mutant's", description: "+10 Damage Resistance and Energy Resistance if you are mutated", restriction: "Armor", restriction_sub: null, restriction_unique: [] },
  { id: "004f6d80", position: 1, name: "Mutant Slayer's", description: "-15% damage from super mutants", restriction: "Armor", restriction_sub: null, restriction_unique: [] },
  { id: "00524143", position: 1, name: "Nocturnal", description: "Damage and Energy Resistance increase with the night and decrease with the day", restriction: "Armor", restriction_sub: null, restriction_unique: [] },
  { id: "00529a09", position: 1, name: "Regenerating", description: "Slowly regenerates health while out of combat", restriction: "Armor", restriction_sub: null, restriction_unique: [] },
  { id: "004f6d7f", position: 1, name: "Troubleshooter's", description: "-15% damage from robots", restriction: "Armor", restriction_sub: null, restriction_unique: [] },
  { id: "0052414a", position: 1, name: "Unyielding", description: "Gain up to +3 to all stats (except END) when low health", restriction: "Armor", restriction_sub: null, restriction_unique: [] },
  { id: "00529a05", position: 1, name: "Vanguard's", description: "Grants up to +35 Energy Resistance and Damage Resistance, the higher your health.", restriction: "Armor", restriction_sub: null, restriction_unique: [] },
  { id: "00529a14", position: 1, name: "Weightless", description: "Weighs 90% less and does not count as armor for the Chameleon mutation", restriction: "Armor", restriction_sub: null, restriction_unique: [] },
  { id: "004ee548", position: 1, name: "Zealot's", description: "-15% damage from Scorched", restriction: "Armor", restriction_sub: null, restriction_unique: [] },
  // Major
  { id: "004f6d85", position: 2, name: "Agility", description: "+1 Agility", restriction: "Armor", restriction_sub: null, restriction_unique: [] },
  { id: "00527f72", position: 2, name: "Antiseptic", description: "+25% Environmental Disease Resistance", restriction: "Armor", restriction_sub: null, restriction_unique: [] },
  { id: "004f6d83", position: 2, name: "Charisma", description: "+1 Charisma", restriction: "Armor", restriction_sub: null, restriction_unique: [] },
  { id: "004f6d82", position: 2, name: "Endurance", description: "+1 Endurance", restriction: "Armor", restriction_sub: null, restriction_unique: [] },
  { id: "00527f6f", position: 2, name: "HazMat", description: "+25 Radiation Resistance", restriction: "Armor", restriction_sub: null, restriction_unique: [] },
  { id: "004f6d84", position: 2, name: "Intelligence", description: "+1 Intelligence", restriction: "Armor", restriction_sub: null, restriction_unique: [] },
  { id: "004f6d86", position: 2, name: "Luck", description: "+1 Luck", restriction: "Armor", restriction_sub: null, restriction_unique: [] },
  { id: "004f6d81", position: 2, name: "Perception", description: "+1 Perception", restriction: "Armor", restriction_sub: null, restriction_unique: [] },
  { id: "00527f6e", position: 2, name: "Poisoner's*", description: "+25 Poison Resistance", restriction: "Armor", restriction_sub: null, restriction_unique: [] },
  { id: "00527f75", position: 2, name: "Powered*", description: "Increases action point refresh speed", restriction: "Armor", restriction_sub: null, restriction_unique: [] },
  { id: "004ee54e", position: 2, name: "Strength", description: "+1 Strength", restriction: "Armor", restriction_sub: null, restriction_unique: [] },
  // Minor
  { id: "00527f79", position: 3, name: "Acrobat's*", description: "Reduces falling damage by 50%", restriction: "Armor", restriction_sub: null, restriction_unique: [] },
  { id: "00527f76", position: 3, name: "Cavalier's", description: "Reduces damage while blocking by 15%", restriction: "Armor", restriction_sub: null, restriction_unique: [] },
  { id: "00527f77", position: 3, name: "Cavalier's* Icon sic", description: "75% chance to reduce damage by 15% while sprinting.", restriction: "Armor", restriction_sub: null, restriction_unique: [] },
  { id: "00527f7a", position: 3, name: "Diver's", description: "Grants the ability to breathe underwater", restriction: "Armor", restriction_sub: null, restriction_unique: [] },
  { id: "0052bdba", position: 3, name: "Durability", description: "50% more durability than normal", restriction: "Armor", restriction_sub: null, restriction_unique: [] },
  { id: "0052bdb7", position: 3, name: "Improved sneaking", description: "Become harder to detect while sneaking", restriction: "Armor", restriction_sub: null, restriction_unique: [] },
  { id: "0052bdb4", position: 3, name: "Reduced ammo weight", description: "Ammo weight reduced by 20%", restriction: "Armor", restriction_sub: null, restriction_unique: [] },
  { id: "0052bdb5", position: 3, name: "Reduced food/drink/chem weight", description: "Food, drink, and chem weights reduced by 20%", restriction: "Armor", restriction_sub: null, restriction_unique: [] },
  { id: "0052bdb6", position: 3, name: "Reduced junk weight", description: "Junk item weights reduced by 20%", restriction: "Armor", restriction_sub: null, restriction_unique: [] },
  { id: "0052bdbc", position: 3, name: "Reduced limb damage", description: "Receive 15% less limb damage", restriction: "Armor", restriction_sub: null, restriction_unique: [] },
  { id: "00527f78", position: 3, name: "Reduced weapon weight", description: "Weapon weights reduced by 20%", restriction: "Armor", restriction_sub: null, restriction_unique: [] },
  { id: "004ee54c", position: 3, name: "Sentinel's*", description: "75% chance to reduce damage by 15% while standing still", restriction: "Armor", restriction_sub: null, restriction_unique: [] },
  { id: "00527f7b", position: 3, name: "Safecracker's*", description: "Increases size of sweet-spot while picking locks", restriction: "Armor", restriction_sub: null, restriction_unique: [] },


    // unique stuff
  { id: "-0000001", position: 0, name: "Cursed", description: "Faster swing speed/fire rate, higher damage, lower durability", restriction: "Weapon", restriction_sub: null, restriction_unique: [{field: "unique_name", selector: "cts", value: "Cursed"}] },
  { id: "-0000002", position: 0, name: "The Fixer", description: "Improved stealth. Faster sneaking movement speed.", restriction: "Weapon", restriction_sub: null, restriction_unique: [{field: "unique_name", selector: "eq", value: "The Fixer"}] },
  { id: "-0000003", position: 0, name: "Voice of Set", description: "Additional electric damage vs. Robots.", restriction: "Weapon", restriction_sub: null, restriction_unique: [{field: "unique_name", selector: "eq", value: "Voice of Set"}] },
  { id: "-0000004", position: 0, name: "The Gutter", description: "Improved damage, gain brief health regeneration when hitting an enemy, weighs less", restriction: "Weapon", restriction_sub: null, restriction_unique: [{field: "unique_name", selector: "eq", value: "The Gutter"}] },

  //
]


export const seed_manager = async (connection: Connection) => {
    await seed_general(connection, legendary_effects, Legendary_Effects).catch(err => console.log(err))
}

const seed_general = async (connection: Connection, array: Array<any>, database) => {
    let repo = connection.getRepository(database)

    let promises = array.map(async(entry) => await repo.save(entry))

    await Promise.all(promises).catch(err => console.log(err))
}