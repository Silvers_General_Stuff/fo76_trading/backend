
export const data_dropdowns = {
    categories:{
        Armor: {
            Wood: {
                Weight: ["Normal"],
            },
            Raider: {
                Weight: ["Normal", "Sturdy", "Heavy"],
            },
            Trapper: {
                Weight: ["Normal"],
            },
            Scout: {
                Weight: ["Normal"],
                Chest: ["Last Bastion"]
            },
            Leather: {
                Weight: ["Normal", "Sturdy", "Heavy"],
                Chest: ["Silver Lining"]
            },
            Metal: {
                Weight: ["Normal", "Sturdy", "Heavy"],
                Chest: ["Trail Warden"]
            },
            Combat: {
                Weight: ["Normal", "Sturdy", "Heavy"],
                Chest: ["Stand Fast"]
            },
            Robot: {
                Weight: ["Normal", "Sturdy", "Heavy"],
            },
            Marine: {
                Weight: ["Normal"],
            },
        },
        Weapon: {
            Ballistic: {
                Pistol: {
                    ".44": ["Medical Malpractice", "Somerset Special", "Voice of Set"],
                    "10mm": ["Anti-Scorched Training Pistol"],
                    "Black Powder": ["Black Powder Blunderbuss"],
                    "Flare": [],
                    "Gauss": [],
                    "Single Action Revolver": ["Fancy Single Action Revolver"],
                    "Western Revolver": [],
                },
                Submachine: {
                    "10mm": ["Perfect Storm", "Old Guard's 10mm SMG"],
                    "Submachine Gun": [],
                },
                Rifle: {
                    "Assault Rifle": ["Whistle in the Dark"],
                    "Black Powder Rifle": ["The Dragon"],
                    "Combat Rifle": ["The Fixer"],
                    "Gauss Rifle": [],
                    "Handmade Rifle": [],
                    "Hunting Rifle": ["Brotherhood Recon Rifle"],
                    "Lever Action Rifle": ["Sole Survivor"],
                    "Radium Rifle": [],
                    "Railway Rifle": [],
                    "Syringer ": ["Rose's Syringer", "Vox Syringer"],
                },
                Shotgun: {
                    "Combat Shotgun": [],
                    "Double-Barrel Shotgun": ["Salt of the Earth"],
                    "Gauss Shotgun": [],
                    "Pump Action Shotgun": ["Civil Unrest", "Fancy Pump Action Shotgun"],
                },
                Pipe: {
                    "Pipe Bolt-Action": [],
                    "Pipe Gun": [],
                    "Pipe Revolver": [],
                },
                Bow: {
                    "Bow": [],
                    "Compound Bow": [],
                    "Crossbow": [],
                },
                Heavy: {
                    "50 cal Machine Gun": ["The Action Hero", "Final Word"],
                    "Broadsider": [],
                    "Gatling Gun": ["Resolute Veteran"],
                    "Harpoon Gun": ["Kingfisher", "Cursed Harpoon Gun"],
                    "Light Machine Gun": [],
                    "Minigun": [],
                },
            },
            Energy: {
                Laser: {
                    "Gatling Laser": [],
                    "Laser Gun": ["Acceptable Overkill"],
                },
                Plasma: {
                    "Plasma Gun": ["Slug Buster"],
                    "Enclave Plasma Gun": [],
                    "Gatling Plasma": [],
                    "Plasma Caster": [],
                },
                Other: {
                    "Alien Blaster": ["The V.A.T.S. Unknown"],
                    "Cryolator": [],
                    "Salvaged Assaultron Head": [],
                    "Tesla Rifle": ["Night Light"],
                },
                Heavy: {
                    "Flamer": ["Pyrolyzer"],
                    "Gauss Minigun": []
                }
            },
            Radiation: {
                Gamma: {
                    "Gamma Gun": []
                }
            },
            Explosive: {
                Heavy: {
                    "Auto Grenade Launcher": [],
                    "Fat Man": ["Daisycutter", "The Guarantee"],
                    "M79 Grenade Launcher": ["Crushing Blow"],
                    "Missile Launcher": ["Bunker Buster"],
                }
            },
            Melee: {
                "1h": {
                    "Assaultron Blade": ["The Gutter"],
                    "Bone Club": [],
                    "Baton": ["Disorderly Conduct"],
                    "Bowie Knife": [],
                    "Chinese Officer Sword": ["Blade of Bastet", "Meteoritic Sword"],
                    "Combat Knife": [],
                    "Commie Whacker": ["Camden Whacker"],
                    "Cultist Blade": ["Nailer"],
                    "Cultist Dagger": [],
                    "Drill": [],
                    "Guitar Sword": [],
                    "Hatchet": [],
                    "Lead Pipe": [],
                    "Machete": [],
                    "Pipe Wrench": ["Mechanic's Best Friend"],
                    "Revolutionary Sword": ["Commander's Charge"],
                    "Ripper": [],
                    "Rolling Pin": [],
                    "Sheepsquatch Club": [],
                    "Shishkebab": [],
                    "Sickle": [],
                    "Ski Sword": ["Black Diamond"],
                    "Switchblade": ["The Quick Fix"],
                    "Tire Iron": [],
                    "Walking Cane": [],
                },
                "2h": {
                    "Super Sledge": ["All Rise"],
                    "Baseball Bat": [],
                    "Board": [],
                    "Bone Hammer": [],
                    "Fire Axe": [],
                    "Golf Club": [],
                    "Grognak's Axe": [],
                    "Mr. Handy Buzz Blade": [],
                    "Multi-purpose Axe": [],
                    "Pickaxe": ["Cursed Pickaxe"],
                    "Pitchfork": [],
                    "Pole Hook": [],
                    "Pool Cue": [],
                    "Protest Sign": [],
                    "Sheepsquatch Staff": [],
                    "Shepherd's Crook": [],
                    "Shovel": ["Cursed Shovel"],
                    "Sledgehammer": [],
                    "Spear": [],
                    "Tenderizer": [],
                    "War drum": [],
                },
                "Unarmed": {
                    "Bear Arm": [],
                    "Boxing Glove": [],
                    "Death Tambo": [],
                    "Deathclaw Gauntlet": ["Unstoppable Monster"],
                    "Gauntlet": [],
                    "Knuckles": [],
                    "Meat Hook": [],
                    "Mole Miner Gauntlet": [],
                    "Power Fist": [],
                    "Chainsaw": [],
                }
            },
        },
    },

    limbs: ["Chest", "Arm-R", "Arm-L", "Leg-R", "Leg-L"],
    level: [1, 5, 10, 15, 20, 25, 30, 35, 40, 45, 50]

}