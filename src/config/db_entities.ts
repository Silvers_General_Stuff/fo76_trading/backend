import {
    Entity,
    Column,
    PrimaryGeneratedColumn,
    PrimaryColumn,
    CreateDateColumn,
    Generated,
    ManyToOne, OneToMany, OneToOne
} from "typeorm";
import {Index, JoinColumn} from "typeorm";

// each entiity is a table


@Entity()
export class Legendary_Effects {
    // https://fallout.fandom.com/wiki/Fallout_76_legendary_effects
    @PrimaryColumn()
    id: string;

    @Column()
    name: string

    // the position of the effect
    @Column()
    position: number

    @Column()
    description: string

    @Column()
    restriction: string

    // only weapons have a sub restriction currently
    @Column({nullable: true})
    restriction_sub: string

    // This is the effect for unique items
    @Column("jsonb")
    restriction_unique: Legendary_Effects_restriction_unique[]
}

export class Legendary_Effects_restriction_unique {
    field: string
    selector: string
    value: string
}

@Entity()
export class Legendary {
    @Column()
    @Generated('increment')
    id?:number

    @PrimaryColumn()
    md5:string

    // first added
    @CreateDateColumn()
    first_added?

    // if the item is unique or not, named weapons
    @Column({nullable: true})
    unique_name: string

    @Column()
    level: number

    // Armor/Weapon
    @Column()
    category: string

    // Weapon: Ballistic/Energy/Radiation/Explosive/Melee
    // Armor: wood/ trapper/.....
    @Column()
    type: string

    // Weapon:  Pistol/Submachine/Rifle/,,,
    // Weapon2: 1h/2h/Unarmed
    // Armor: chest/arm-r/arm-l/...
    @Column()
    type_sub: string

    // Weapon:  .44/10mm/Western Revolver
    // Weapon2: Assaultron Blade/Baton
    // Armor: Normal/Sturdy/Heavy
    @Column()
    category_specific: string

    // effect0 is for unique weapons only
    @Column({ name: 'effect_0', nullable: true })
    effect_0?: string;
    @ManyToOne(() => Legendary_Effects,{nullable: true})
    @JoinColumn({ name: 'effect_0' })
    effect0?: Legendary_Effects

    @Column({ name: 'effect_1', nullable: true })
    effect_1?: string;
    @ManyToOne(() => Legendary_Effects,{nullable: true})
    @JoinColumn({ name: 'effect_1' })
    effect1?: Legendary_Effects

    @Column({ name: 'effect_2', nullable: true })
    effect_2?: string;
    @ManyToOne(() => Legendary_Effects,{nullable: true})
    @JoinColumn({ name: 'effect_2' })
    effect2?: Legendary_Effects

    @Column({ name: 'effect_3', nullable: true })
    effect_3?: string;
    @ManyToOne(() => Legendary_Effects,{nullable: true})
    @JoinColumn({ name: 'effect_3' })
    effect3?: Legendary_Effects

    // not currently in use
    @Column({ name: 'effect_4', nullable: true })
    effect_4?: string;
    @ManyToOne(() => Legendary_Effects,{nullable: true})
    @JoinColumn({ name: 'effect_4' })
    effect4?: Legendary_Effects

    @Column({ name: 'effect_5', nullable: true })
    effect_5?: string;
    @ManyToOne(() => Legendary_Effects,{nullable: true})
    @JoinColumn({ name: 'effect_5' })
    effect5?: Legendary_Effects
}

@Entity()
export class User_Steam {
    @PrimaryColumn()
    id: string

    @CreateDateColumn()
    first_added?

    @Column()
    provider: string

    @Column()
    displayName: string

}
@Entity()
export class User_Discord {
    @PrimaryColumn()
    id: string

    // first added
    @CreateDateColumn()
    first_added?

    @Column()
    provider: string

    @Column()
    username: string

    @Column()
    discriminator: string
}

@Entity()
export class User_Reddit {
    @PrimaryColumn()
    id: string

    // first added
    @CreateDateColumn()
    first_added?

    @Column()
    provider: string

    @Column()
    name: string
}

@Entity()
export class User {
    // from steam
    @PrimaryColumn()
    id: string

    // from steam
    @Column()
    displayName: string

    @Column({ default: "No Fo76 name set" })
    fo76_name?: string

    @Column({ default: "No platform set" })
    platform?: string

    @Column({ type: "jsonb", default: [] })
    fo76_names_past?: string[]

    // has game
    @Column({ default: false })
    has_game?: boolean

    // first added
    @CreateDateColumn()
    first_added?

    // from here on are stuff for teh site
    // listigns that a user has
    @OneToMany(() => Legendary_Listing, listing => listing.seller)
    listings_legendary_sell: Legendary_Listing[]

    @OneToMany(()  => Legendary_Listing, listing => listing.buyer)
    listings_legendary_buy: Legendary_Listing[]

    // completed trades
    @Column({ default: 0 })
    trades_completed?: number

    @Column({ default: 0 })
    trades_completed_buy?: number

    @Column({ default: 0 })
    trades_completed_sell?: number

    // Intergrations

    // steam
    @OneToOne(() => User_Steam)
    @JoinColumn()
    steam: User_Steam

    // discord
    @OneToOne(() => User_Discord, {nullable: true})
    @JoinColumn()
    discord?: User_Discord

    // reddit
    @OneToOne(() => User_Reddit, {nullable: true})
    @JoinColumn()
    reddit?: User_Reddit
}

@Entity()
export class Legendary_Listing {
    @PrimaryGeneratedColumn("uuid")
    id?: string;

    @ManyToOne(()  => User,  {nullable: false})
    @JoinColumn()
    seller?: User;

    @ManyToOne(()  => User,  {nullable: true})
    @JoinColumn()
    buyer?: User;

    @Column("timestamp")
    start?: Date;

    @Column("timestamp", {nullable: true})
    end?: Date;

    // Draft/Active/Sold - Pending/Sold/Delisted
    @Index()
    @Column()
    status?: "Draft" | "Active" | "Pending" | "Cancelled" | "Completed" | "Want"

    @ManyToOne(()  => Legendary, {nullable: true})
    @JoinColumn()
    item?: Legendary

    @Column()
    quantity?: number;

    @Column("json")
    wanted?: Array<Wanted>

    @Column({nullable: true})
    note?:string
}



@Entity()
export class Item {
    @PrimaryGeneratedColumn()
    id: number;

    @Column()
    name: string
}
export class Currency {
    name: string
}

@Entity()
export class Legendary_Offer {
    @PrimaryColumn()
    @Generated('increment')
    id?:number

    @CreateDateColumn()
    date?

    @ManyToOne(() => Legendary_Listing)
    listing: Legendary_Listing

    @ManyToOne(() => User)
    user_seller: User

    @ManyToOne(() => User)
    user_buyer: User
}

export class Wanted {
    // item (ammo/scrap/flux) /
    // Legendary / Item / Currency / List
    type: string
    link:  string
    quantity: number
}

// this is an array of all the entities
export const entities = [
    Legendary_Effects,
    Legendary,
    User,
    User_Steam,
    User_Discord,
    User_Reddit,
    Legendary_Listing,
    Legendary_Offer
   // Wanted
    //Listing,

    //Item
];