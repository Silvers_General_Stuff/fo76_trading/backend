import passport from "passport"
import config from "./config";
import {Strategy as Strategy_Steam} from "passport-steam"
import {Strategy as Strategy_Jwt} from "passport-jwt"
import {Strategy as Strategy_Discord} from "passport-discord"
import {Strategy as Strategy_Reddit} from "passport-reddit"
import {Connection} from "typeorm";
import {User, User_Discord, User_Reddit, User_Steam} from "./db_entities"
import axios from "axios"

export default function (connection:Connection){
    // these are used to sign up
    passport.use(oauth_steam(connection));
    // this is used after signup/login to access data
    passport.use(jwtGen(connection));

    // example of multiple intergrations from https://github.com/ooade/next-apollo-auth

    // Discord
    passport.use(oauth_discord(connection));
    // reddit
    passport.use(oauth_reddit(connection))

    passport.serializeUser((user, cb) => {cb(null, user)});
    passport.deserializeUser((obj, cb) => {cb(null, obj)});

    return passport
}

interface data_steam {
    response:{
        games: data_game[]
    }
}
interface data_game {
    appid: number
    playtime_forever:number
    playtime_windows_forever: number
    playtime_mac_forever: number
    playtime_linux_forever: number
}

// steam is teh core of teh login
function oauth_steam(connection: Connection) {
    return new Strategy_Steam({
            returnURL: config.sso.steam.url,
            realm: config.sso.steam.id,
            apiKey: config.sso.steam.secret,
        },
        async (identifier, profile_steam: User_Steam, done) => {
            // check to see if teh user already exists
            let user = await connection.getRepository(User).find({id: profile_steam.id}).catch((err) => {console.log(err);return []});

            let profile: User

            // if not then create the profile
            if (user.length === 0) {
                // disabling signups
                //return done(new Error("Not accepting signups"))

                // save teh steam profile
                await connection.getRepository(User_Steam).save(profile_steam).catch(err => {console.log(err)})

                profile = new User()
                profile.id = profile_steam.id
                profile.displayName = profile_steam.displayName
                profile.has_game = await check_Steam_game(profile_steam.id)
                profile.steam = profile_steam

                await connection.getRepository(User).save(profile).then(() => true).catch(err => {console.log(err);return false})

            } else {
                profile = user[0]
            }


            return done(null, profile)
        })
}

function jwtGen(connection: Connection){
    const cookieExtractor = (req) => {
        let token = null;
        if (req && req.cookies) {
            token = req.cookies['jwt'];
        }
        return token;
    };
    let opts = {
        jwtFromRequest: cookieExtractor,
        secretOrKey: config.jwtSecret,
    };
    return new Strategy_Jwt(opts, async(jwt_payload, done)=> {
        let results = await connection.getRepository(User).find({id:jwt_payload.id}).catch(err => {console.log(err);return []});

        if(results.length === 0){
            return done(new Error("No user"))
        }else{
            return done(null, results[0])
        }
    })
}

async function check_Steam_game(id): Promise<boolean>{
    let url = `https://api.steampowered.com/IPlayerService/GetOwnedGames/v0001/?key=${config.steam_key}&steamid=${id}&format=json`
    let profile_data:data_steam = await axios.get(url).then(result => result.data).catch(err => {console.log(err); return {response:{games:[]}}})
    let fo76 = profile_data.response.games.filter(game => game.appid === 1151340)
    return fo76.length !== 0;
}

function oauth_discord(connection: Connection) {
    return new Strategy_Discord({
            clientID: config.sso.discord.id,
            clientSecret: config.sso.discord.secret,
            callbackURL: config.sso.discord.url,
            scope: ["identify"],
            passReqToCallback: true
        },

        async (req, accessToken, refreshToken,  profile_discord: User_Discord, done) => {
            if (!req.user) {
                return done(Error("Not previously authenticated"));
            }

            // save teh steam profile
            await connection.getRepository(User_Discord).save(profile_discord).catch(err => {console.log(err)})

            let profile: User = req.user
            profile.discord = profile_discord

            await connection.getRepository(User).save(profile).then(() => true).catch(err => {console.log(err);return false})

            return done(null, profile)
        })

}

function oauth_reddit(connection: Connection){
    return new Strategy_Reddit({
            clientID: config.sso.reddit.id,
            clientSecret: config.sso.reddit.secret,
            callbackURL: config.sso.reddit.url,
            passReqToCallback: true
        },

        async (req, accessToken, refreshToken,  profile_reddit: User_Reddit, done) => {
            if (!req.user) {
                return done(Error("Not previously authenticated"));
            }

            // save teh steam profile
            await connection.getRepository(User_Reddit).save(profile_reddit).catch(err => {console.log(err)})

            let profile: User = req.user
            profile.reddit = profile_reddit

            await connection.getRepository(User).save(profile).then(() => true).catch(err => {console.log(err);return false})

            return done(null, profile)
        })


}