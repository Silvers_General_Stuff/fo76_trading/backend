import dotenv from 'dotenv'
import packageData from '../../package.json'

dotenv.config();


interface Config {
  name: string;
  version: string
  env: string
  port: number
  frontend: string
  db:{
    db_name: string
    db_port: number
    db_host: string
    db_user: string
    db_pass: string
  }
  sessionSecret: string
  jwtSecret: string
  steam_key:string
  sso:{
    [propName: string]: SSO
  }
}

interface SSO {
  id: string
  secret: string
  url: string
}

const config: Config = {
  name: packageData.name,
  version: packageData.version,
  env: process.env.NODE_ENV || process.env.enviroment,
  port: parseInt(process.env.REACT_APP_backend_port, 10) || 8080,
  frontend: process.env.REACT_APP_frontend_address,
  db: {
    db_name: process.env.POSTGRES_DB,
    db_port: parseInt(process.env.db_port) || 5432,
    db_host: process.env.db_host,
    db_user: process.env.POSTGRES_USER,
    db_pass: process.env.POSTGRES_PASSWORD,
  },
  sessionSecret: process.env.sessionSecret,
  jwtSecret: process.env.jwtSecret,
  steam_key: process.env.steam_key,
  sso: {
    steam: {
      id: process.env.steam_clientID,
      secret: process.env.steam_clientSecret,
      url: process.env.steam_callbackURL
    },
    discord: {
      id: process.env.discord_clientID,
      secret: process.env.discord_clientSecret,
      url: process.env.discord_callbackURL
    },
    reddit: {
      id: process.env.reddit_clientID,
      secret: process.env.reddit_clientSecret,
      url: process.env.reddit_callbackURL
    },
  }
};
export default config
