import "reflect-metadata";
import {Connection, createConnection} from "typeorm";

import config from "./config";
import { entities } from "./db_entities";
import {seed_manager} from "./db_seeding"

export async function liftDB(): Promise<Connection>{
    let connection = await createConnection({
        name: new Date().toISOString(),
        type: "postgres",
        host: config.db.db_host,
        port: config.db.db_port,
        username: config.db.db_user,
        password: config.db.db_pass,
        database: config.db.db_name,
        entities: entities,
        synchronize: true,
        logging: false
    }).catch(error => {console.log(error); process.exit(1)});

    await seed_manager(connection)

    return connection
}
