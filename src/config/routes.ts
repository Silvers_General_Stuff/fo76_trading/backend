import auth from "../routes/auth"
import itemData from "../routes/itemData"
import listings from "../routes/listings"
import account from "../routes/account"
import offers from "../routes/offers"

export default (ctx) => {
    auth(ctx);
    itemData(ctx);
    listings(ctx);
    account(ctx);
    offers(ctx);
}