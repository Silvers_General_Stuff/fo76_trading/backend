import {Legendary, Legendary_Effects, User, Wanted} from "../config/db_entities";
import {data_dropdowns} from "../config/data"
import {Connection} from "typeorm";
import {liftDB} from "../config/db";
import md5 from "md5"
import {Submitted_Object} from "../routes/listings";

export class Weapon_Object {
    category?: string
    level?: number
    specific?:string
    type?:string
    type_sub?:string
    unique?: boolean
    unique_name?:string
    selected_effects?: string[]
}

export class Listing_Object {
    seller: User
    buyer?: User
    listing_id?: number
    quantity?: number
    status?: string
    wanted?: Wanted[]
    note?: string
}

export function compare( a:object, b:object, field:string ) {
    if ( a[field] < b[field] ){
        return -1;
    }
    if ( a[field] > b[field] ){
        return 1;
    }
    return 0;
}

export async function get_user(id:string): Promise<User[]>{
    let connection:Connection = await liftDB();

    let user:User[] = await connection
        .getRepository(User)
        .find({ id:id},)
        .catch(err => {console.log(err); return []})

    await connection.close();

    return user
}

export async function get_general(connection:Connection, table, filter:object = {}): Promise<Array<any>>{
    return await connection
        .getRepository(table)
        .find(filter)
        .catch(err => {console.log(err);return []})
}

export function beautifyJSON (json, min) {
    if (min === "min") {
        return JSON.stringify(json)
    } else {
        return JSON.stringify(json, null, '  ')
    }
}

export async function get_legendary (connection:Connection, legendary_object_data: Submitted_Object): Promise<Legendary>{
    let want = legendary_object_data.status === "Want"

    // create initial legendary object
    let legendary:Legendary = {
        category: "",
        level: 0,
        type: "",
        type_sub: "",
        unique_name: null,
        category_specific: "",
        md5: ""
    }

    // check basic data
    // level
    if(data_dropdowns.level.indexOf(legendary_object_data.level) !== -1){
        legendary.level = legendary_object_data.level
    }

    // category - always selected, even on wishlists
    let category = Object.keys(data_dropdowns.categories)
    if(category.indexOf(legendary_object_data.category) !== -1){
        legendary.category = legendary_object_data.category
    }else{
        // invalid category selected
        return legendary
    }


    // type
    if(legendary_object_data.type) {
        let type = Object.keys(data_dropdowns.categories[legendary.category])
        if (type.indexOf(legendary_object_data.type) !== -1) {
            legendary.type = legendary_object_data.type
        } else {
            if (!want) {
                // invalid category selected
                return legendary
            }
        }


        // type_sub
        if(legendary_object_data.type_sub) {
            let type_sub
            if (legendary.category === "Armor") {
                type_sub = data_dropdowns.limbs
            } else {
                type_sub = Object.keys(data_dropdowns.categories[legendary.category][legendary.type])
            }
            if (type_sub.indexOf(legendary_object_data.type_sub) !== -1) {
                legendary.type_sub = legendary_object_data.type_sub
            } else {
                // invalid type_sub selected
                return legendary
            }

            // category_specific
            if(legendary_object_data.specific) {
                let category_specific
                if (legendary.category === "Armor") {
                    category_specific = data_dropdowns.categories[legendary.category][legendary.type].Weight
                } else {
                    category_specific = Object.keys(data_dropdowns.categories[legendary.category][legendary.type][legendary.type_sub])
                }

                if (category_specific.indexOf(legendary_object_data.specific) !== -1) {
                    legendary.category_specific = legendary_object_data.specific
                } else {
                    // invalid category_specific selected
                    return legendary
                }

                // legendary names
                // unique_name
                // get a list of the allowed names for that subgroup
                if(legendary_object_data.unique_name) {
                    let unique_name
                    if (legendary.category === "Armor") {
                        let unique_name_tmp = data_dropdowns.categories[legendary.category][legendary.type][legendary.type_sub]
                        if (typeof unique_name_tmp === "undefined") {
                            unique_name = []
                        } else {
                            unique_name = unique_name_tmp
                        }
                    } else {
                        unique_name = data_dropdowns.categories[legendary.category][legendary.type][legendary.type_sub][legendary.category_specific]
                    }
                    if (unique_name.indexOf(legendary_object_data.unique_name) !== -1) {
                        legendary.unique_name = legendary_object_data.unique_name
                    }
                }
            }
        }
    }

    // finally legendary effects
    // make sure they are in a nice order
    //weapon_object.selected_effects.sort((a,b)=> compare(a,b, "position"))

    let used_positions = []
    // get legendary effects
    let effects: Legendary_Effects[] = await connection.getRepository(Legendary_Effects).find().catch(err => {console.log(err);return []})
    let for_hash = [legendary_object_data.category, legendary_object_data.level, legendary_object_data.type, legendary_object_data.type_sub, legendary_object_data.specific, legendary_object_data.unique_name]
    for(let i=0;i<legendary_object_data.selected_effects.length;i++){
        let selected_effect = legendary_object_data.selected_effects[i]

        // check if effect exists
        let db_effects = effects.filter(item => item.name === selected_effect)

        if(db_effects.length === 0){continue}
        let db_effect = db_effects[0]

        // check if position is already filled
        if(used_positions.indexOf(db_effect.position) !== -1){continue}

        // check restrictions

        // check if it can be applied to the category: Weapon/Armor
        if(db_effect.restriction !== legendary_object_data.category){continue}

        // check if it can apply to the subcategory
        if(db_effect.restriction_sub){
            // this is in the case of general ranged
            if(db_effect.restriction_sub === "Ranged"){
                if(legendary_object_data.specific === "Melee"){continue}
            }else{
                if(db_effect.restriction_sub !== legendary_object_data.specific){continue}
            }
        }


        // this is for unique legendaries
        if(db_effect.restriction_unique.length >0){
            let valid = true
            // {field: "name", selector: "cts", value: "Cursed"}
            for(let j=0;j<db_effect.restriction_unique.length;j++){
                let restrictions = db_effect.restriction_unique[j]

                let valid_tmp = false
                switch(restrictions.selector){
                    case "cts" :{
                        valid_tmp = legendary[restrictions.field].indexOf(restrictions.value) !== -1;
                        break
                    }
                    case "eq" :{
                        valid_tmp = legendary[restrictions.field] === restrictions.value;
                        break
                    }
                    default:{
                        valid_tmp = false
                    }
                }

                if(!valid_tmp){
                    valid = false
                }
            }

            if(!valid){
                continue
            }
        }

        // to get here the slot isnt filled, the name and position is correct
        // add to effects
        legendary[`effect${db_effect.position}`] = db_effect

        // lastly fill teh spot
        used_positions.push(db_effect.position)
        for_hash.push(db_effect.id)
    }

    legendary.md5 = md5(for_hash.join(""))

    await connection.getRepository(Legendary).save(legendary).catch(err => console.log(err))

    return legendary
}