// this is to output the idem and stat data

import {get_general, beautifyJSON} from "../scripts/functions";
import {Legendary, Legendary_Effects} from "../config/db_entities";
import {data_dropdowns} from "../config/data"
import {ctx} from "../index"

export default (ctx: ctx) => {
    let {server, connection} = ctx

    server.get('/legendary/item',async (req, res, next)=>{
        let result = await get_general(connection, Legendary,req.query.filter)
        res.header("Content-Type",'application/json');
        res.send(beautifyJSON(result, req.query.beautify));
        next()
    })
    server.get('/legendary/effect',async (req, res, next)=>{
        let result = await get_general(connection, Legendary_Effects,req.query.filter)
        res.header("Content-Type",'application/json');
        res.send(beautifyJSON(result, req.query.beautify));
        next()
    })

    // dropdowns for the frontend
    server.get('/legendary/dropdowns',async (req, res, next)=>{
        res.header("Content-Type",'application/json');
        res.send(beautifyJSON(data_dropdowns, req.query.beautify));
        next()
    })
}
