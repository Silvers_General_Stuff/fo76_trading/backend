import {ctx} from "../index"
import {Legendary_Listing, User} from "../config/db_entities";
import {Any, Connection, Not} from "typeorm";
import {offer_get_buy, offer_get_my} from "./offers";
import {Result_Object} from "./listings";
export default (ctx: ctx) => {
    const {passport, connection, server} = ctx

    // this is for getting the users own account info
    server.get('/account',passport.authenticate('jwt', { session: false }), async (req, res, next) => {
        let result = await account_get_own(connection, req.user)
        res.send(result)
        next()
    })

    // this is for getting the users own account info
    server.post('/account',passport.authenticate('jwt', { session: false }), async (req, res, next) => {
        if(typeof req.body === "undefined"){
             console.log({result: "error", error: "no body"})
        }
        let result = await account_update(connection, req.user, req.body)
        res.send(result)
        next()
    })

    server.get('/account/:id',
        // this is to see if a person is logged in or not
        (req, res, next) => {
            passport.authenticate('jwt', (err, user) => {
                if (err) {return next()}
                if (!user) {return next()}

                // NEED TO CALL req.login()!!!
                return req.login(user, next);
            })(req, res, next);
        },
        async (req, res, next) => {
            let result = await account_get(connection, req.params.id, req.user)
            res.send(result)
            next()
        })
}

async function account_get_own (connection: Connection, user:User): Promise<User[]>{
    let select = [
        "user.id", "user.displayName", "user.has_game", "user.first_added", "user.fo76_name","user.fo76_names_past","user.platform",
        "steam.displayName", "steam.first_added",
        "discord.username", "discord.first_added",
        "reddit.name", "reddit.first_added",
    ]
    return account_get_general (connection, user.id, select)
}

async function account_get_general (connection: Connection, user_id:string, select: string[] = ["user.id"]): Promise<User[]>{
    return await connection.getRepository(User)
        .createQueryBuilder("user")
        .leftJoin("user.steam", "steam")
        .leftJoin("user.discord", "discord")
        .leftJoin("user.reddit", "reddit")
        .select(select)
        .where(`user.id = :user`, { user: user_id })
        .getMany()
        .catch(err => {console.log(err);return []})
}

async function account_get (connection: Connection, user_id: string, user?:User){
    let response = {
        account_Data: [],
        current: {
            // current offer list
            sell: [],
            // current wishlist
            buy: []
        },
        history: {
            // items sold
            sell: [],
            // items bought
            buy: []
        },
        pending: {
            // offers on my items
            sell: [],
            // my offers on other items
            buy: []
        }

    }
    let select = [
        "user.id", "user.displayName", "user.has_game", "user.first_added", "user.platform",
        "user.trades_completed","user.trades_completed_buy","user.trades_completed_sell",
        "user.fo76_name","user.fo76_names_past",
        "steam.displayName",
        "discord.username","discord.discriminator","discord.id",
        "reddit.name",
    ]

    let requests: any[] = [

        // gets wishlist
        await listings_helper(connection, {  seller:{ id: user_id },status: "Want" }).then(result => response.current.buy = result).catch(err => {console.log(err)}),

        // gets items previouslty bought or sold
        await listings_helper(connection, {  seller:{ id: user_id },status: "Completed" }).then(result => response.history.sell = result).catch(err => {console.log(err)}),
        await listings_helper(connection, {  buyer:{ id: user_id },status: "Completed" }).then(result => response.history.buy = result).catch(err => {console.log(err)}),
        await account_get_general(connection,user_id,select).then(result => response.account_Data = result).catch(err => {console.log(err)})

    ]


    // logged in, handle first
    if(user){
        // logged in user seeing their own stuff
        if(user.id === user_id){
            // bids, only shown to the two users who tehya re for
            requests.push(await offer_get_buy(connection,user).then(result => response.pending.sell = result))
            requests.push(await offer_get_my(connection,user).then(result => response.pending.buy = result))

            // selelr can see all their current transactions
            requests.push(listings_helper(connection, { seller:{ id: user.id },status: Not(Any(["Completed","Want", "Cancelled"])) }).then(result => response.current.sell = result).catch(err => {console.log(err)}))
        }else{
            // logged in user viewing anoter users stuff
            requests.push(listings_helper(connection, {seller:{ id: user_id },status: "Active" }).then(result => response.current.sell = result).catch(err => {console.log(err)}))
        }
    }else{
        // loged out user viewing anotehr users stuff
        requests.push(listings_helper(connection, {seller:{ id: user_id },status: "Active" }).then(result => response.current.sell = result).catch(err => {console.log(err)}))
    }

    await Promise.all(requests).catch(err => console.log(err))

    return response
}

export function listings_helper (connection: Connection, query, select?: string[]){
    if(!select){
        select  = [
            // all teh data fromt eh listing
            "listing",
            // information on teh buyer
            "buyer.id", "buyer.displayName",
            // infoirmation on teh seller
            "seller.id", "seller.displayName","seller.platform",
            // the legendary
            "item.md5"
        ]
    }

    return connection
        .getRepository(Legendary_Listing)
        .createQueryBuilder("listing")
        .leftJoin("listing.buyer", "buyer")
        .leftJoin("listing.seller", "seller")
        .leftJoin("listing.item", "item")
        .where(query)
        .select(select)
        .getMany()
        .catch(err => {console.log(err);return []})
}

async function account_update(connection: Connection, user:User, newData: { fo76_name?:string, platform?:string }): Promise<Result_Object>{
    if(!newData){return {status: "error", error:"Could not update account, no data"}}

    if(newData.fo76_name){
        user.fo76_name = newData.fo76_name
        user.fo76_names_past.push(newData.fo76_name)
    }
    if(newData.platform){
        user.platform = newData.platform
    }

    return await connection.getRepository(User)
        .save(user)
        .then(()=>{return {status: "success", success:"Account updated"}})
        .catch(err => {console.log(err); return {status: "error", error:"Could not update account"}})
}