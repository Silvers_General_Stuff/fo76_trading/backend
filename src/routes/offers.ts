import {ctx} from "../index"
import {Connection} from "typeorm";
import {Legendary_Listing, Legendary_Offer, User} from "../config/db_entities";
import {Result_Object} from "./listings";
// handle offers

export default (ctx: ctx) => {
    let {server, connection, passport} = ctx


    // a buyer basically says tehre are interested in teh item.
    server.post('/offers',passport.authenticate('jwt', { session: false }), async (req, res, next) => {
        if(typeof req.body === "undefined"){
            return res.send({result: "error", error: "no body"})
        }
        let result = await offer_add(connection,req.user, req.body.listing_id)

        res.send(result)
        next()
    })

    // a buyer says they are no longer intereested in teh item
    server.delete('/offers',passport.authenticate('jwt', { session: false }), async (req, res, next) => {
        if(typeof req.body === "undefined"){
            return res.send({result: "error", error: "no body"})
        }
        let result = await offer_remove(connection,req.user, req.body.listing_id)

        res.send(result)
        next()
    })

    // Seller comfirms the transaction, bumps up counts for both users, marks the transaction as completed
    server.patch('/offers',passport.authenticate('jwt', { session: false }), async (req, res, next) => {
        if(typeof req.body === "undefined"){
            return res.send({result: "error", error: "no body"})
        }
        let result = await offer_accept(connection,req.user, req.body.offer_id)

        res.send(result)
        next()
    })

    // get listings for the current user
    server.get('/offers',passport.authenticate('jwt', { session: false }), async (req, res, next) => {
        if(typeof req.body === "undefined"){
            return res.send({result: "error", error: "no body"})
        }
        res.send({
            buy_offers: await offer_get_buy(connection, req.user),
            my_offers: await offer_get_my(connection, req.user)
        })
        next()
    })

    server.get('/offers/my',passport.authenticate('jwt', { session: false }), async (req, res, next) => {
        if(typeof req.body === "undefined"){
            return res.send({result: "error", error: "no body"})
        }
        res.send(await offer_get_my(connection, req.user))
        next()
    })

    server.get('/offers/buy',passport.authenticate('jwt', { session: false }), async (req, res, next) => {
        if(typeof req.body === "undefined"){
            return res.send({result: "error", error: "no body"})
        }
        res.send(await offer_get_buy(connection, req.user))
        next()
    })
}


async function offer_add(connection: Connection, user: User, listing_id: string): Promise<Result_Object>{
    // get the listing
    let listing_existing:Legendary_Listing[] = await connection.getRepository(Legendary_Listing)
        .createQueryBuilder("listing_legendary")
        .leftJoinAndSelect("listing_legendary.seller", "seller")
        .where(`listing_legendary.id = :id`, { id: listing_id })
        .getMany()
        .catch(err => {console.log(err);return []})


    // check if the lsiting exists
    if(listing_existing.length === 0){return {status: "error", error:"no listing with that id"}}

    let listing = listing_existing[0]

    // check if the listing is active
    if(listing.status !== "Active"){return {status: "error", error:"Not an active listing"}}

    // check if the user is not teh owner of the listing
    if(listing.seller.id === user.id){return {status: "error", error:"Cannot make offer on own listing"}}


    // check if user has not already submitted a listing
    let offer_existing:Legendary_Offer[] = await connection.getRepository(Legendary_Offer)
        .find({ listing:listing,user_buyer:user })
        .catch(err => {console.log(err);return []})
    if(offer_existing.length !== 0){return {status: "success", success:"Legendary_Offer saved"}}

    // then add the offer
    let offer:Legendary_Offer = {
        listing: listing,
        user_seller: listing.seller,
        user_buyer: user
    }

    return await connection.getRepository(Legendary_Offer).save(offer)
        .then(()=>{return {status: "success", success:"Legendary_Offer saved"}})
        .catch(err => {console.log(err); return {status: "error", error:"Could not save offer"}})
}

async function offer_remove(connection: Connection, user: User, listing_id: string): Promise<Result_Object>{
    // get the listing
    let listing_existing:Legendary_Listing[] = await connection.getRepository(Legendary_Listing)
        .createQueryBuilder("listing_legendary")
        .leftJoinAndSelect("listing_legendary.seller", "seller")
        .where(`listing_legendary.id = :id`, { id: listing_id })
        .getMany()
        .catch(err => {console.log(err);return []})


    // check if the lsiting exists
    if(listing_existing.length === 0){return {status: "error", error:"no listing with that id"}}

    let listing = listing_existing[0]

    // check if the user is not teh owner of the listing
    if(listing.seller.id === user.id){return {status: "error", error:"Cannot make offer on own listing"}}


    // check if user has not already submitted a listing
    let offer_existing:Legendary_Offer[] = await connection.getRepository(Legendary_Offer)
        .find({ listing:listing,user_buyer:user })
        .catch(err => {console.log(err);return []})
    if(offer_existing.length === 0){return {status: "error", error:"No previous offer"}}

    return await connection.getRepository(Legendary_Offer)
        .delete(offer_existing[0].id).then(()=>{return {status: "success", success:"Legendary_Offer deleted"}})
        .catch(err => {console.log(err); return {status: "error", error:"Could not delete offer"}})
}

async function offer_accept(connection: Connection, seller: User, offer_id: number): Promise<Result_Object>{
    // user is teh owner of the transaction

    // get the offer
    let offer_existing:Legendary_Offer[] = await connection.getRepository(Legendary_Offer)
        .createQueryBuilder("offers")
        .leftJoin("offers.user_seller", "user_seller")
        .leftJoinAndSelect("offers.user_buyer", "user_buyer")
        .leftJoinAndSelect("offers.listing", "listing")
        .where(`offers.id = :id`, { id: offer_id })
        .andWhere(`user_seller.id = :user_seller`, { user_seller: seller.id })
        .getMany()
        .catch(err => {console.log(err);return []})

    // check if the lsiting exists
    if(offer_existing.length === 0){return {status: "error", error:"No offer with that id"}}

    let offer = offer_existing[0]

    return await connection
        .transaction(async (transaction_manager) => {
            let transaction_connection = transaction_manager.connection

            // bumps up counts for both users
            await transaction_connection.createQueryBuilder()
                .update(User)
                .set({
                    trades_completed: () => "trades_completed + 1",
                    trades_completed_sell: () => "trades_completed_sell + 1",
                })
                .where("id = :seller_id", {seller_id: seller.id}).execute();

            await transaction_connection.createQueryBuilder()
                .update(User)
                .set({
                    trades_completed: () => "trades_completed + 1",
                    trades_completed_buy: () => "trades_completed_buy + 1",
                })
                .where("id = :buyer_id", {buyer_id: offer.user_buyer.id}).execute();


            // marks the transaction as completed: Completed
            await transaction_connection.createQueryBuilder()
                .update(Legendary_Listing)
                .set({
                    status: "Completed",
                    end: new Date(),
                    buyer: offer.user_buyer
                })
                .where("id = :listing_id", {listing_id: offer.listing.id}).execute();

            // deletes teh offers for the transacrtion
            await transaction_connection.createQueryBuilder()
                .delete()
                .from(Legendary_Offer)
                .where("listing = :listing", {listing: offer.listing.id})
                .execute();

        })
        .then(()=>{return {status: "success", success:"Legendary_Offer completed"}})
        .catch(err => {console.log(err); return {status: "error", error:"Could not delete offer"}})

}

export async function offer_get_buy (connection: Connection, user: User):Promise<Legendary_Offer[]>{
    return await connection.getRepository(Legendary_Offer)
        .createQueryBuilder("offers")
        .leftJoin("offers.user_seller", "user_seller")
        .leftJoin("offers.user_buyer", "user_buyer")
        .leftJoin("offers.listing", "listing")
        .select(["offers.id", "offers.date", "user_buyer.id", "user_buyer.displayName", "listing"])
        .where(`user_seller.id = :user_seller`, { user_seller: user.id })
        .getMany()
        .catch(err => {console.log(err);return []})
}

export async function offer_get_my (connection: Connection, user: User):Promise<Legendary_Offer[]>{
    return await connection.getRepository(Legendary_Offer)
        .createQueryBuilder("offers")
        .leftJoin("offers.user_seller", "user_seller")
        .leftJoin("offers.user_buyer", "user_buyer")
        .leftJoin("offers.listing", "listing")
        .select(["offers.id", "offers.date", "user_seller.id", "user_seller.displayName", "listing"])
        .where(`user_buyer.id = :user_buyer`, { user_buyer: user.id })
        .getMany()
        .catch(err => {console.log(err);return []})
}


