
// this is to output the idem and stat data

import { beautifyJSON, get_legendary } from "../scripts/functions";
import { Legendary_Effects, Legendary_Listing, User, Wanted } from "../config/db_entities";
import {listings_helper} from "./account"
import {ctx} from "../index"
import {Connection} from "typeorm";

export class Submitted_Object {
    // listing specific
    quantity?: number
    status?: "Draft" | "Active" | "Pending" | "Cancelled" | "Completed" | "Want"
    wanted?: Wanted[]
    note?: string
    listing_id?: string

    // weapon specific
    category?: string
    level?: number
    specific?:string
    type?:string
    type_sub?:string
    unique?: boolean
    unique_name?:string
    selected_effects?: string[]
}
/*
?filter[status]=Active&filter[listing_id]=1&filter[category]=Weapon&filter[type]=Ballistic
 */
export class Result_Object {
    status: string
    success?:string
    error?:string
}

export default (ctx: ctx) => {
    let {server, connection, passport} = ctx

    // create new listings
    server.post('/listing',passport.authenticate('jwt', { session: false }), async (req, res, next) => {
        if(typeof req.body === "undefined"){
            return res.send({result: "error", error: "no body"})
        }
        let result = await legendary_listing_add(connection,req.user, req.body)

        res.send(result)
        next()
    })

    // updates a listing, both teh seller and buyer
    server.patch('/listing',passport.authenticate('jwt', { session: false }), async (req, res, next) => {
        if(typeof req.body === "undefined"){
            return res.send({result: "error", error: "no body"})
        }
        let result = await legendary_listing_update(connection,req.user, req.body)

        res.send(result)
        next()
    })


    // get listings
    server.get('/listing', async (req, res, next) => {
        let result = await listings_helper(connection, { status: "Active" })
        res.header("Content-Type",'application/json');
        res.send(beautifyJSON(result, req.query.beautify));
        next()
    })


    // offer
}

async function legendary_listing_add (connection: Connection, seller:User,  submitted:Submitted_Object): Promise<Result_Object>{
    let legendary = await get_legendary(connection, submitted)
    if(legendary.md5 === ""){
        // invalid legendary
        return {status: "error", error:"Invalid Legendary"}
    }

    let listing: Legendary_Listing = {
        seller: seller,
        item: legendary,
        start: new Date()
    }

    let valid_status = ["Draft", "Active", "Pending", "Cancelled", "Want"]

    // set defaults if its not in the object passed through
    if(submitted.status && valid_status.indexOf(submitted.status) !== -1){
        listing.status = submitted.status
    }else{
        listing.status = "Draft"
    }

    listing.quantity = submitted.quantity ? submitted.quantity : 1
    listing.wanted = submitted.wanted ? submitted.wanted : []
    listing.note = submitted.note ? submitted.note : ""

    return await connection.getRepository(Legendary_Listing)
        .save(listing)
        .then(()=>{return {status: "success", success:"Listing saved"}})
        .catch(err => {console.log(err); return {status: "error", error:"Could not save listing"}})
}

async function legendary_listing_update (connection: Connection, user:User, submitted:Submitted_Object): Promise<Result_Object>{
    if(!submitted.listing_id){return {status: "error", error:"no listing_id"}}

    // check if supplied user owns the listing
    let listing_existing:Legendary_Listing[] = await listings_helper(connection, { id:submitted.listing_id})

    if(listing_existing.length === 0){return {status: "error", error:"no listing with that id"}}

    let listing = listing_existing[0]


    if( listing.status === "Completed" ){
        //if the listing is completed  then no-one can edit it

        return {status: "error", error:"Listing is already complete"}

    }else if( user.id === listing.seller.id){
        // user is seller
        // can update

        // status
        // quantity
        // wanted
        // note

        let valid_status = ["Draft", "Active", "Pending", "Cancelled"]

        if(submitted.status && valid_status.indexOf(submitted.status) !== -1){listing.status = submitted.status}
        if(submitted.quantity){listing.quantity = submitted.quantity}
        if(submitted.wanted){listing.wanted = submitted.wanted}
        if(submitted.note){listing.note = submitted.note}


        return await connection.getRepository(Legendary_Listing).save(listing)
            .then(()=>{return {status: "success", success:"Listing updated"}})
            .catch(err => {console.log(err); return {status: "error", error:"Could not update listing"}})

    } else {
        // user is a random
        // no-no

        return {status: "error", error:"Unauthorised"}
    }
}

async function legendary_listing_find (connection: Connection, query: Submitted_Object = {status: "Active"} ): Promise<Legendary_Listing[]>{
    let builder = connection
        .getRepository(Legendary_Listing)
        .createQueryBuilder("listing_legendary")
        .leftJoin("listing_legendary.item", "legendary")

    // for teh changfe between where and andWhere
    let where = false

    // weapon specific
    let search_categories_weapon = ["category", "level", "specific", "type","type_sub", "unique","unique_name"]
    for(let i=0;i<search_categories_weapon.length;i++){
        let category = search_categories_weapon[i]
        if(query[category]){
            if(!where){
                builder.where(`legendary.${category} = :${category}`, { [category]: query[category] })
                where = true
            }else{
                builder.andWhere(`legendary.${category} = :${category}`, { [category]: query[category] })
            }
        }
    }

    // selected_effects
    if(query.selected_effects){
        if(typeof query.selected_effects === "string"){
            query.selected_effects = [query.selected_effects]
        }

        let effects = await connection.getRepository(Legendary_Effects).find().catch(err => {console.log(err);return []})
        for(let i=0;i< query.selected_effects.length;i++ ){
            let selected_effect = query.selected_effects[i]

            let db_effects = effects.filter(item => item.name === selected_effect)
            if(db_effects.length === 0){continue}

            let searchCol = `effect${db_effects[0].position}`
            if(!where){
                builder.where(`legendary.${searchCol} = :${searchCol}`, { [searchCol]: db_effects[0].id })
                where = true
            }else{
                builder.andWhere(`legendary.${searchCol} = :${searchCol}`, { [searchCol]: db_effects[0].id })
            }
        }
    }

    // listing specofic
    let search_categories_listing = ["seller_id", "buyer_id", "status"]
    for(let i=0;i<search_categories_listing.length;i++){
        let category = search_categories_listing[i]
        if(query[category]){
            if(!where){
                builder.where(`listing_legendary.${category} = :${category}`, { [category]: query[category] })
                where = true
            }else{
                builder.andWhere(`listing_legendary.${category} = :${category}`, { [category]: query[category] })
            }
        }
    }

    return await builder.getMany().catch(err => {console.log(err);return []})
}

async function legendary_listing_find_basic (connection: Connection ): Promise<Legendary_Listing[]>{
     return await connection.getRepository(Legendary_Listing).find({ status: "Active" }).catch(err => {console.log(err); return []})
}