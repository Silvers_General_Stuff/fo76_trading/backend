import jwt from 'jsonwebtoken'
import config from "../config/config";
import {User} from "../config/db_entities";
import {ctx} from "../index"
import {Connection} from "typeorm";

export default (ctx:ctx) => {
    const {server, connection, passport} = ctx

    let success = config.frontend + "#/login";
    let failure = config.frontend + "#/login";

    server.get('/auth/steam',processGeneral(passport, 'steam'));
    server.get('/auth/steam/callback', processGeneralCallback(passport, "steam", failure), redirectWithToken(success));

    server.get('/auth/discord', processGeneral(passport, 'discord'));

    server.get('/auth/discord/callback',
        // first verify the jwt
        passport.authenticate('jwt', { session: false }),
        // then handle the result
        passport.authenticate("discord", { failureRedirect: failure }),
        // no need to set cookie again, redirect straight back to account
        (req, res) => {return res.status(200).redirect(`${config.frontend}#/account`)}
    );

    server.delete('/auth/discord',
        passport.authenticate('jwt', { session: false }),
        async (req, res, next) => {await unlink_discord(connection, req.user); next()},
        (req, res) => {return res.status(200).send()}
    );

    server.get('/auth/reddit',
        passport.authenticate("reddit", { failureRedirect: failure, state: Math.random().toString(36) })
    );

    server.get('/auth/reddit/callback',
        // first verify the jwt
        passport.authenticate('jwt', { session: false }),
        // then handle the result
        passport.authenticate("reddit", { failureRedirect: failure }),
        // no need to set cookie again, redirect straight back to account
        (req, res) => {return res.status(200).redirect(`${config.frontend}#/account`)}
    );

    server.delete('/auth/reddit',
        passport.authenticate('jwt', { session: false }),
        async (req, res, next) => {await unlink_reddit(connection, req.user); next()},
        (req, res) => {return res.status(200).send()}
    );
}

const processGeneral = (passport, provider:string, object?:object) => {
    if(object){
        return passport.authenticate(provider, object)
    }
    return  passport.authenticate(provider)
};

const processGeneralCallback = (passport, provider:string, failure:string) => {
    return  passport.authenticate(provider, { failureRedirect: failure })
};

const redirectWithToken =  (success:string) =>{
    return (req, res) => {
        return res
            .status(200)
            // this is the actual jwt
            .cookie('jwt', signToken(req.user.id), {
                domain:"fo76.ie",
                httpOnly: true,
                secure: true,
                //sameSite: "Lax"
            })
            // this is to let the browser know that the user is logged in, no other purporse
            .cookie('loggedIn', true, {
                domain:"fo76.ie",
                httpOnly: false,
                secure: true,
                //sameSite: "Lax"
            })
            .redirect(success)
    }
};

const signToken = user => {
    return jwt.sign({ id: user }, config.jwtSecret, {expiresIn: 604800})
};

async function unlink_discord (connection: Connection, user: User){
    user.discord = null
    await connection.getRepository(User).save(user).then(() => true).catch(err => {console.log(err)})

}
async function unlink_reddit (connection: Connection, user: User){
    user.reddit = null
    await connection.getRepository(User).save(user).then(() => true).catch(err => {console.log(err)})

}