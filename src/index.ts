import express from "express";
import routes from "./config/routes"
import config from "./config/config"
import { liftDB } from "./config/db"
import passportSetup from "./config/passport"
import session from "express-session"
import cookieParser  from "cookie-parser"
import bodyParser from "body-parser"
import { Connection } from "typeorm";
import cors from "cors"

main();
async function main(){
    const server = express();

    let connection: Connection = await liftDB();
    let passport = passportSetup(connection);

    server.use(cors({
        origin:[
            // testing env
            "http://localhost",
            "http://localhost:3000", "http://localhost:8089",
            "http://192.168.1.13:3000", "http://192.168.1.13:8091",
            // production
            "https://fo76.ie", "https://www.fo76.ie",
        ],
        credentials: true
    }))

    server.use(session({ secret:config.sessionSecret }));

    // get cookies
    server.use(cookieParser());

    // get the body
    // parse application/x-www-form-urlencoded
    server.use(bodyParser.urlencoded({ extended: false }))
    // parse application/json
    server.use(bodyParser.json())

    server.use(passport.initialize());
    server.use(passport.session());

    // start the Express server
    server.listen( config.port, async () => {
        routes({connection: connection, server: server, passport:passport});
        console.log( `server started at http://localhost:${ config.port }` );
    } );
}

export class ctx {
    connection: Connection
    server: any
    passport: any
}